import { deleteMoney, addRemoveClass } from "./functions.js";
import { rotateDrum, getTargetBlock, checkTargetItem } from './script.js';

const preloader = document.querySelector('.preloader');

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	let targetElement = e.target;

	if (targetElement.closest('.header__privacy ') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

	if (targetElement.closest('.drum__button')) {
		if (+sessionStorage.getItem('money') >= 250) {
			rotateDrum();
			deleteMoney(250, '.score', 'money');
			addRemoveClass('.drum__button', '_hold', 2500);

			setTimeout(() => {
				let block = getTargetBlock();
				checkTargetItem(block);
			}, 2100);
		}

	}
})